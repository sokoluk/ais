# Install
### Clone repository
    git clone git@gitlab.com:sokoluk/ais.git
    cd ais/ais

### Create virtual Enviroment
    python3 -m venv `path`

### Create virtual Enviroment
    `path`/bin/pip3 install -r requirements.txt

### Check Docker service is running or run it.
    sudo systemctl start docker

### Check DB exists and create it if needed
- Connect to postgres
```bash
psql -h localhost -U postgres
```
execute in psql promt ↓
- Check database ais2 in list
```
\list
```
- if not exists, create db
```SQL
create database ais2;
```
exit psql promt

### Migrate db
```bash
python3 manage.py migrate
```

