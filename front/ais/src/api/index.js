import {AuthResource} from './resources'
import moment from 'moment'
moment.locale('ru')

export default {
  localLogin: function (data) {
    return AuthResource.save({id: 'local'}, data)
  }
}
export {moment}
