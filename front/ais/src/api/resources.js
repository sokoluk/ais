import Vue from 'vue'
import VueResource from 'vue-resource'
import {API_ROOT} from '../settings/Settings'
import { getCookie, signOut, isLogin } from '../utils/authService'

Vue.use(VueResource)

Vue.http.options.crossOrigin = true
Vue.http.options.credentials = true

Vue.http.interceptors.push((request, next) => {
  request.headers = request.headers || {}
  if (isLogin()) {
    request.headers.set('Authorization', 'JWT ' + getCookie('token').replace(/(^")|("$)/g, ''))
  } else {
    console.log('not login')
    request.headers.delete('Access-Control-Allow-Headers')
    request.headers.delete('Authorization')
  }
  next((response) => {
    if (response.status === 401) {
      console.log('auth is 401 - FAIL')
      signOut()
      window.location = '/#/auth/login/'
    }
  })
})

export const OdocResource = Vue.resource(API_ROOT + 'odocs{/id}{/controller}')
export const DocFlow = Vue.resource(API_ROOT + 'docflow{/docid}{/controller}')
export const DocFlowStages = Vue.resource(API_ROOT + 'docflow/docflowstage{/stageid}')
export const OdocListResource = Vue.resource(API_ROOT + 'odocs/')
export const OdocTypeListResource = Vue.resource(API_ROOT + 'odocs/odoctypes')
export const ProfileListResource = Vue.resource(API_ROOT + 'profile/list{/subdivision}/')
export const ProfileMe = Vue.resource(API_ROOT + 'profile/me')
export const AuthResource = Vue.resource(API_ROOT + 'profile/auth/token/')
