// Исползуем компонент для событий
// https://vuejs.org/v2/guide/migration.html#dispatch-and-broadcast-replaced
import Vue from 'vue'

var eventHub = new Vue()
export default eventHub
