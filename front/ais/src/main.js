import Vue from 'vue'
import App from './App.vue'

import VueQuillEditor from 'vue-quill-editor'
import BootstrapVue from 'bootstrap-vue'

import router from './router'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import DocFlowTree from './components/documents/DocFlowTree.vue'

Vue.use(VueQuillEditor)
Vue.use(BootstrapVue)
Vue.component('DocFlowTree', DocFlowTree)

Object.defineProperty(Vue.prototype, '$bus', {
  get () {
    return this.$root.bus
  }
})

var bus = new Vue({})

new Vue({    // eslint-disable-line no-new
  el: '#app',
  router,
  data: {
    bus: bus
  },
  render: h => h(App)
})
