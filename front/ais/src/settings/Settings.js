
export default ({
  'api-address': 'http://ais-dev.sokoluk.com:8000/api/'
})
// mute debug console.log
console.log(process.env.NODE_ENV)
if (process.env.NODE_ENV === 'production') {
  console = console || {} // eslint-disable-line no-native-reassign, no-global-assign
  console.log = function () {}
}

export const API_ROOT = (process.env.NODE_ENV === 'production') ? 'http://ais-dev.sokoluk.com:8000/api/' : 'http://localhost:8000/api/'
export const CookieDomain = (process.env.NODE_ENV === 'production') ? '.sokoluk.com' : ''
