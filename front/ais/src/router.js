import Vue from 'vue'
import VueRouter from 'vue-router'

import DocumentList from './components/documents/DocumentList.vue'
import DocumentView from './components/documents/DocumentView.vue'
import DocFlowView from './components/documents/DocFlowView.vue'
import ProfileLoginView from './components/profiles/ProfileLogin.vue'
import ProfileTree from './components/profiles/profile_tree/ProfileTree.vue'
import Dashboard from './components/profiles/Dashboard.vue'
import {isLogin} from './utils/authService'

Vue.use(VueRouter)

const router = new VueRouter({
  routes: [
    {
      path: '/',
      name: 'dashboard',
      component: Dashboard
    },
    {
      path: '/documents',
      name: 'documentListView',
      component: DocumentList
    },
    {
      path: '/document/:id',
      name: 'documentView',
      component: DocumentView
    },
    {
      path: '/docflow/:docid',
      name: 'docflowView',
      component: DocFlowView
    },
    {
      path: '/auth/login',
      name: 'login',
      component: ProfileLoginView
    },
    {
      path: '/profiles',
      name: 'profiles',
      component: ProfileTree
    }
    // TODO NOT FOUND COMPONENT NEED
    //   {
    //     path: '*',
    //     component: NotFound
    //   }
  ]
})

export default router
