import re

space_tag = re.compile('(</.*?>)|(&nbsp;)')
non_space_tag = re.compile('<.*?>|&amp;')
TITLE_LENGHT = 70


def cleanhtml(raw_html):
    cleantext = re.sub(space_tag, ' ', raw_html)
    return re.sub(non_space_tag, '', cleantext)


def get_title(doc_body):
    """generate title from document body
    """
    replaced=cleanhtml(doc_body)
    formated_text = replaced[:TITLE_LENGHT]
    if len(formated_text) > TITLE_LENGHT - 3:
        formated_text = formated_text+'...'
    return formated_text
