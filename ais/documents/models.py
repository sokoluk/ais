"""odels for Documents"""
from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils import timezone


class Document(models.Model):
    """
    Базовый документ, содержит общие поля для всех документов
    """
    author = models.ForeignKey(
        'auth.User',
        null=False,
        on_delete=models.PROTECT
    )
    created_at = models.DateTimeField(
        default=timezone.now,
        verbose_name='Дата создания'
    )
    changed_at = models.DateTimeField(
        auto_now=True,
        verbose_name='Дата изменения'
    )
    doc_type = models.CharField(
        null=False,
        max_length=100,
        blank=False,
    )
    related_documents = models.ManyToManyField('Document')


class BaseModelDocument(models.Model):
    """Base model of Document for all types"""
    class Meta:
        """Abstract Meta class of django model"""
        abstract = True

    @receiver(pre_save)
    def create_document(sender, instance, **kwargs):
        """
        Создаем базовый документ
        """
        if issubclass(sender, BaseModelDocument):
            if instance.document_id is None:
                instance.document = Document.objects.create(
                    doc_type=instance.__class__,
                    author=instance.author,
                )
            else:
                instance.document.save()

    @property
    def author(self):
        """Author of Document"""
        return self.document.author

    # @property
    # def docflow(self):
    #     """
    #     Схема согласования документа
    #     """
    #     doc_docflow = docflow.DocFlow.objects.filter(document_id=self.pk)
    #     return doc_docflow[0] if doc_docflow else None

    # @property
    # def docflow_tree(self):
    #     """
    #     Дерево согласования документа
    #     """
    #     docflow_tree = docflow.DocFlowStage.objects.filter(
    #         docflow=self.docflow
    #     ).get_descendants(include_self=True)
    #     return docflow_tree

    # @property
    # def active_docflow_stage(self):
    #     """
    #     Получить активные этапы

    #     Returns:
    #         list|None: Список активных этапов
    #     """
    #     if self.id:
    #         LOGGER.debug('get docflow stage %r', self.id)
    #         doc_docflow = docflow.DocFlow.objects.filter(document=self)
    #         if doc_docflow:
    #             active_stages = docflow.DocFlowStage.objects.filter(
    #                 docflow_id=doc_docflow[0].pk,
    #                 status=0
    #             )
    #             return active_stages[0] if active_stages else None
    #     return None
