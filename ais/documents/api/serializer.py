"""Serializer for Base Document"""
import datetime
import time

from rest_framework import serializers

from odocs.models import Document
from odocs.models import Odoc
from profiles.api.serializers import ProfileSerializer


class DocumentSerializer(serializers.ModelSerializer):
    author = serializers.SerializerMethodField()
    changed_at = serializers.SerializerMethodField()
    created_at = serializers.SerializerMethodField()

    def get_author(self, obj):
        return ProfileSerializer(obj.author).data

    def get_changed_at(self, obj):
        return time.mktime(obj.document.changed_at.timetuple())

    def get_created_at(self, obj):
        return time.mktime(obj.document.created_at.timetuple())

    def save(self, **kwargs):
        document_ = None
        if not self.instance:
            document_ = Document(
                doc_type=self.Meta.model.DOCTYPE_NAME,
                author=kwargs.get('author')
            )
            document_.save()
        else:
            document_ = self.instance.document
        return super().save(document=document_)


class DocumentListSerializer(DocumentSerializer):
    def get_author(self, obj):
        return ProfileSerializer(obj.author).data.get('short_name')
