from django import forms
from django.contrib import admin
from .models import Document


class DocumentForm(forms.ModelForm):
    def save(self, *args, **kwargs):
        if not self.instance.document_id:
            document_ = Document(
                doc_type=self.instance.__class__.__name__,
                author=self.instance.author,
            )
            document_.save()
            self.instance.document = document_
        return super().save(*args, **kwargs)


class DocumentAdmin(admin.ModelAdmin):
    exclude = ['document']
    form = DocumentForm
