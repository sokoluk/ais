"""
The main configuration URL for the application with API
"""
from django.conf.urls import (
    url,
    include,
)

from dashboard.api import urls as dashboard_api
from odocs.api import urls as odocs_api
from profiles.api import urls as profiles_api
from docflows.api import urls as docflows_api


urlpatterns = [
    url(r'^auth/', include('rest_auth.urls')),
    url(r'^auth/registration/', include('rest_auth.registration.urls')),
    url(r'^odocs/', include((odocs_api, 'odoc-api'), namespace='odoc-api')),
    url(r'^docflow/', include((docflows_api, 'docflow-api'), namespace='docflow-api')),
    url(r'^profile/', include((profiles_api, 'profiles-api'), namespace='profiles-api')),
]
