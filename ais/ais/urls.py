"""ais URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import (
    url,
    include,
)
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import TemplateView

from profiles import urls as profiles_url
from odocs import urls as odocs_url
from dashboard.views import DashBoardView
from . import urls_api

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^profiles/', include((profiles_url, 'profiles'), namespace='profiles')),
    url(r'^dashboard/$', DashBoardView.as_view(), name='dashboard'),
    url(r'^odocs/', include((odocs_url, 'odocs'), namespace='odocs')),
    url(r'^$', TemplateView.as_view(template_name='index.html'), name='home'),
    url(r'^api/', include((urls_api, 'api'), namespace='api')),
]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
