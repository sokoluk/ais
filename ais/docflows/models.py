"""DocFlow models"""
from rest_framework import exceptions
from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.utils import timezone
from mptt.models import MPTTModel, TreeForeignKey

from documents.models import Document

class NotAllowSetNextStage(exceptions.APIException):
    """Нет прав на действия с этапами"""



class DocPath(models.Model):
    """
    Модель схемы согласования
    """
    name = models.CharField(max_length=100)

    def __str__(self):
        return 'DocPath: %s' % self.name


class DocPathStage(models.Model):
    """
    Модель этапа согласования
    """
    name = models.CharField(max_length=100)
    docpath = models.ForeignKey('DocPath', on_delete=models.CASCADE)

    def __str__(self):
        return 'DocPathStage (pk =%r fk=%r): %s' % (self.pk, self.docpath.pk, self.name)


class DocFlow(models.Model):
    """
    Класс процесса согласования документа
    """
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    created_at = models.DateTimeField(
        default=timezone.now
    )
    changed_at = models.DateTimeField(
        auto_now=True
    )
    document = models.ForeignKey(Document, on_delete=models.CASCADE)
    doc_id = models.IntegerField(null=False)
    doc_type = models.CharField(max_length=100)

    def __str__(self):
        return 'DocFlow %s' % self.pk


class DocFlowStage(MPTTModel):
    """
    Этап согласования документа
    """
    docflow = models.ForeignKey('DocFlow', models.CASCADE)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True, on_delete=models.CASCADE)

    docpathstage = models.ForeignKey('DocPathStage', on_delete=models.PROTECT)
    docpathbindstage = models.ForeignKey('DocPathBindStage', on_delete=models.PROTECT)
    # чьё действие привело к появлению этапа
    author = models.ForeignKey('auth.User', related_name='author', on_delete=models.PROTECT)
    # кому этап принадлежит, в чью зону ответсвенности попал
    recipient = models.ForeignKey('auth.User', related_name='recipient', null=True, on_delete=models.PROTECT)
    # Кто этап провел или отменил
    signer = models.ForeignKey('auth.User', related_name='signer', null=True, on_delete=models.PROTECT)
    # комментарий к этапу
    comment = models.TextField(null=True,)
    status = models.IntegerField(default=0)
    created_at = models.DateTimeField(
        default=timezone.now
    )
    changed_at = models.DateTimeField(
        auto_now=True
    )

    def __str__(self):
        return 'DocFlowStage pk=%r doc_id=%s' % (self.pk, self.docflow.doc_id)

    @property
    def name(self):
        """
        Название этапа
        """
        return self.docpathstage.name

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

    def get_next_stages(self):
        """следующие этапы по умолчанию"""
        docflow_tree = self.docpathbindstage.get_children()
        return docflow_tree

    def check_jump(self):
        """check access to change status"""
        if self.status != 0 and any([_child.status != 0 for _child in self.get_children()]):
            raise exceptions.MethodNotAllowed('Нет доступа на переход', 405)


    def check_action_rights(self, action):
        """check rights to change this stage for current user"""
        self.check_jump()
        # TODO Будем определят права на доступ действий
        # permission_on_stage(
        #     self.doc_path_id,
        #     self.docpathbindstage,
        #     self.signer,
        #     self.recipient,
        # )

    def agree(self, signer, comment):
        """Согласовать этап"""
        self.check_action_rights('agree')
        # Повторное согласование, меняем только коммент
        if self.status == 1:
            self.comment = comment
            self.save()
            return
        # пересоздадим несогласованные этапы
        if self.status != 0 and self.get_children():
            for _child in self.get_children():
                _child.delete_last()
        self.status = 1
        self.signer = signer
        self.comment = comment
        self.save()
        for stage in self.get_next_stages():
            DocFlowStage(
                docflow=self.docflow,
                parent=self,
                author=signer,
                docpathbindstage=stage,
                docpathstage=stage.docpath_stage,
            ).save()

    def disagree(self, signer, comment, stage=None):
        """Вернуть на доработку"""
        # TODO Реализовать
        pass

    def decline(self, signer, comment):
        """Отклонить согласование"""
        # TODO Реализовать
        pass


    def delete_last(self):
        """Удалить последний этап"""
        if not self.get_children():
            if self.parent:
                parent = self.parent
                parent.status = 0
                parent.signer = None
                parent.save()
                self.delete()
        else:
            raise exceptions.MethodNotAllowed('Нет доступа на удаление', 405)


class DocPathBindStage(MPTTModel):
    """
    Модель связывающая этап и схему согласования
    """
    name = models.CharField(max_length=100)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True, on_delete=models.CASCADE)
    docpath_stage = models.ForeignKey('DocPathStage', on_delete=models.PROTECT)
    ord_level = models.IntegerField()
    next_stages = ArrayField(
        models.IntegerField(),
        null=True,
        blank=True,
    )
    prev_stages = ArrayField(
        models.IntegerField(),
        null=True,
        blank=True,
    )
    priority = models.IntegerField(default=0)

    def __str__(self):
        return 'DocPathBindStage pk=%r docpathstage=%s' % (self.pk, self.docpath_stage.name)

    @classmethod
    def initial_stage(cls, doc_path_id):
        """
        Первый этап схемы
        """
        return cls.objects.filter(docpath_stage__docpath_id=doc_path_id)[0].get_root()
