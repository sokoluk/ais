from django.dispatch import receiver
from django.db.models.signals import post_save
from documents.models import BaseModelDocument
from docflows.models import (
    DocFlow,
    DocFlowStage,
    DocPathBindStage,
)

class DocflowMixin():
    """Миксин для создания этапов документа"""
    @property 
    def doc_path(self):
        raise NotImplementedError

    @receiver(post_save)
    def new_document(sender, instance, created, **kwargs):
        """
        Создание нового документа, создание схемы согласования,
        первого этапа схемы
        """
        if issubclass(sender, BaseModelDocument) and created:
            document_docflow = DocFlow(
                document=instance.document,
                doc_id=instance.pk,
                doc_type=instance.__class__,
                author=instance.document.author,
            )
            document_docflow.save()
            init_stage = DocPathBindStage.initial_stage(instance.doc_path)
            dfs = DocFlowStage(
                docflow=document_docflow,
                author=instance.document.author,
                docpathbindstage=init_stage,
                docpathstage=init_stage.docpath_stage,
            )
            dfs.save()
