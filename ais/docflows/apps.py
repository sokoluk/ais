from django.apps import AppConfig


class DocflowsConfig(AppConfig):
    name = 'docflows'
