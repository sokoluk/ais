"""View for DocFlow and DocFlowStages"""

from rest_framework import generics

from docflows import models as docflow_models

from . import serializers


class DocFlowStageListAPIView(generics.ListAPIView):
    """List DocFlowStages for document"""
    serializer_class = serializers.DocFlowStageSerializer

    def get_queryset(self):
        """
        Дерево согласования документа
        """
        tree = docflow_models.DocFlowStage.objects.all().filter(
            docflow__doc_id=self.kwargs.get('doc_id'),
            parent=None
            )
        return tree


class DocPathStageNextListAPIView(generics.ListAPIView):
    """List of available Next stages"""
    serializer_class = serializers.DocPathBindStageSerializer

    def get_queryset(self):
        next_stages = docflow_models.DocFlowStage.objects.get(
            pk=self.kwargs.get('docflowstage_id')
        ).get_next_stages()
        return next_stages

class DocFlowStageAPIView(generics.RetrieveUpdateAPIView):
    """View for set status current stage"""
    serializer_class = serializers.DocFlowStageActionSerializer
    def get_object(self):
        docflow = docflow_models.DocFlowStage.objects.get(pk=self.kwargs.get('docflowstage_id'))
        return docflow

    def perform_update(self, serializer):
        serializer.save(author=self.request.user)
