from django.conf.urls import url, include
from rest_framework import routers

from . import views


urlpatterns = [
    url(r'^(?P<doc_id>\d+)$', views.DocFlowStageListAPIView.as_view(), name='list'),
    url(r'^docflowstage/(?P<docflowstage_id>\d+)/next_stages$', views.DocPathStageNextListAPIView.as_view(), name='next_stages'),
    url(r'^docflowstage/(?P<docflowstage_id>\d+)$', views.DocFlowStageAPIView.as_view(), name='agree'),
]
