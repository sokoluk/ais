"""Serializers for DocFlow"""
import datetime

from rest_framework import serializers

from docflows import models as docflow_models
from django.contrib.auth.models import User
from profiles.api.serializers import ProfileSerializer
from rest_framework_recursive.fields import RecursiveField


ACCEPT = 1
IN_WORK = 0
STATUS = {
    IN_WORK: 'В работе',
    ACCEPT: 'Согласован',
}


class ActionField(serializers.ChoiceField):
    def get_attribute(self, obj):
        return 'agree'

    def to_representation(self, obj):
        """
        Serialize the object's class name.
        """
        return obj

    def to_internal_value(self, data):
        return data

class StatusField(serializers.ChoiceField):
    def get_attribute(self, obj):
        return {
            'id': obj.status,
            'name': STATUS.get(obj.status)
        }

    def to_representation(self, obj):
        """
        Serialize the object's class name.
        """
        return obj

    def to_internal_value(self, data):
        return data


class DocPathStageSerializer(serializers.ModelSerializer):
    class Meta:
        model = docflow_models.DocPathStage
        fields = ('name', 'pk')


class DocPathBindStageSerializer(serializers.ModelSerializer):
    class Meta:
        model = docflow_models.DocPathBindStage
        fields = (
            'pk', 'name', 'parent', 'docpath_stage', 'ord_level',
            'next_stages', 'prev_stages', 'priority'
        )

class DocFlowStageSerializer(serializers.ModelSerializer):
    docpathstage = serializers.SerializerMethodField()
    author = serializers.SerializerMethodField()
    signer = serializers.SerializerMethodField()
    recipient = serializers.SerializerMethodField()
    changed_at = serializers.SerializerMethodField()
    created_at = serializers.SerializerMethodField()
    children = serializers.SerializerMethodField()
    status = StatusField(choices=list(STATUS.keys()))

    class Meta:
        model = docflow_models.DocFlowStage
        fields = (
            'pk', 'children', 'docflow', 'parent', 'docpathstage', 'recipient', 'signer', 'comment',
            'docpathbindstage', 'author', 'status', 'created_at', 'changed_at', 'level'
        )

    def get_children(self, obj):
        if obj.children:
            return DocFlowStageSerializer(obj.children, many=True).data
        return None

    def get_docpathstage(self, obj):
        return DocPathStageSerializer(obj.docpathstage).data

    def get_author(self, obj):
        return ProfileSerializer(obj.author).data

    def get_recipient(self, obj):
        return ProfileSerializer(obj.recipient).data

    def get_signer(self, obj):
        return ProfileSerializer(obj.signer).data

    def get_changed_at(self, obj):
        return datetime.datetime.strftime(obj.changed_at, '%d.%m.%Y %H:%M')

    def get_created_at(self, obj):
        return datetime.datetime.strftime(obj.created_at, '%d.%m.%Y %H:%M')


class DocFlowStageActionSerializer(DocFlowStageSerializer):
    action = ActionField(choices=['agree', 'disagree', 'delete', 'decline'])
    class Meta:
        model = docflow_models.DocFlowStage
        fields = ('docpathstage', 'comment', 'action')

    def update(self, instance, post_data, *args, **kwargs):
        action = post_data.get('action')
        author = post_data.get('author')

        if action == 'agree':
            instance.agree(author, post_data.get('comment'))
        elif action == 'disagree':
            instance.disagree(author, post_data.get('comment'))
        elif action == 'delete':
            instance.delete_last()
        elif action == 'decline':
            instance.decline(author, post_data.get('comment'))
        return instance
