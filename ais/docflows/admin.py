from django.contrib import admin

from . import models

admin.site.register(models.DocPath)
admin.site.register(models.DocPathStage)

admin.site.register(models.DocFlow)
admin.site.register(models.DocFlowStage)
admin.site.register(models.DocPathBindStage)


