from django.db import models
from ckeditor.fields import RichTextField

from documents.models import (
    BaseModelDocument,
    Document,
)
from docflows.models import DocPath
from docflows.mixins import DocflowMixin


class OdocType(models.Model):
    title = models.TextField(max_length=100, verbose_name='Тип служебной записки')
    doc_path = models.ForeignKey(DocPath, on_delete=models.PROTECT)

    def __str__(self):
        return self.title


class Odoc(DocflowMixin, BaseModelDocument):
    DOCTYPE_NAME = 'ODOC'
    text = RichTextField(verbose_name='Содержание')
    document = models.OneToOneField(
        Document,
        on_delete=models.CASCADE,
    )
    odoc_type = models.ForeignKey(OdocType, on_delete=models.PROTECT)

    class Meta:
        ordering = ('-document__changed_at', '-document__created_at', )

    def __str__(self):
        return 'Odoc pk={} {}'.format(self.pk, self.odoc_type.title)

    @property
    def doc_path(self):
        return self.odoc_type.doc_path
