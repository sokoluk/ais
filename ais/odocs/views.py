from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse
from django.urls import reverse_lazy
from django.views import generic

from .forms import OdocForm
from .models import Odoc


class CreateOdoc(LoginRequiredMixin, generic.CreateView):
    template_name = 'odocs/create_form.html'
    form_class = OdocForm

    def get_success_url(self):
        return reverse('odocs:edit', args=(self.object.id,))

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class ListOdoc(LoginRequiredMixin, generic.ListView):
    template_name = 'odocs/list_form.html'
    form_class = OdocForm
    model = Odoc
    # select_related = ('document',)

    def get_objects(self, queryset=None):
        return Odoc.objects.all().select_relate()


class EditOdoc(LoginRequiredMixin, generic.UpdateView):
    template_name = 'odocs/create_form.html'
    form_class = OdocForm

    def get_queryset(self):
        return Odoc.objects.all().select_related('document', 'document__author', 'odoc_type')

    def get_success_url(self):
        return reverse_lazy('odocs:list')


class DeleteOdoc(generic.DeleteView):
    model = Odoc
    success_url = reverse_lazy('odocs:list')

    def get(self, *args, **kwargs):
        """
        This has been overriden because by default
        DeleteView doesn't work with GET requests
        """
        return self.delete(*args, **kwargs)
