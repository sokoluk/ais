from django.apps import AppConfig


class OdocsConfig(AppConfig):
    name = 'odocs'
