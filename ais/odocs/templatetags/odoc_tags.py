from django import template

from ..forms import OdocForm

register = template.Library()

@register.inclusion_tag('odocs/_form.html', takes_context=True)
def odoc_form(context):
    form = OdocForm()
    return {'form': form}
