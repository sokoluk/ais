from django.contrib import admin
from . import models
from . import forms

class OdocFormAdmin(admin.ModelAdmin):
    exclude = ['document']
    form = forms.OdocForm

admin.site.register(models.OdocType)
admin.site.register(models.Odoc)
