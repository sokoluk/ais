from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^$', views.ListOdoc.as_view(), name='list'),
    url(r'^create/$', views.CreateOdoc.as_view(), name='create'),
    url(r'^(?P<pk>\d+)/delete/$', views.DeleteOdoc.as_view(), name='delete'),
    url(r'^(?P<pk>\d+)/$', views.EditOdoc.as_view(), name='edit'),
]
