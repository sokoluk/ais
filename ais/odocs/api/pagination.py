from documents.api.pagination import DocumentLimitOffsetPagination

class OdocLimitOffsetPagination(DocumentLimitOffsetPagination):
    # default_limit = 2
    max_limit = 100
