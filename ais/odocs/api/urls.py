from django.conf.urls import url, include
from rest_framework import routers

from . import views as views


urlpatterns = [
    url(r'^$', views.OdocListAPIView.as_view(), name='list'),
    url(r'^(?P<pk>\d+)$', views.OdocDetailAPIView.as_view(), name='detail'),
    url(r'^create$', views.OdocCreateAPIView.as_view(), name='create'),
    url(r'^(?P<pk>\d+)/edit$', views.OdocUpdateAPIView.as_view(), name='edit'),
    url(r'^(?P<pk>\d+)/delete$', views.OdocDeleteAPIView.as_view(), name='delete'),
    url(r'^odoctypes$', views.OdocTypeListAPIView.as_view(), name='odoc_type_list'),
]
