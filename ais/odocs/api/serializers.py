from rest_framework import serializers
from odocs.models import Odoc
from odocs.models import OdocType
from documents.api.serializer import DocumentSerializer
from documents.api.serializer import DocumentListSerializer
from documents import utils


class OdocTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = OdocType
        fields = ('id', 'title', 'doc_path')


class OdocSerializer(DocumentSerializer):
    odoc_type = serializers.SerializerMethodField()

    class Meta:
        model = Odoc
        fields = ('id', 'text', 'odoc_type', 'author')

    def get_odoc_type(self, obj):
        return OdocTypeSerializer(obj.odoc_type).data


class OdocEditSerializer(DocumentSerializer):

    class Meta:
        model = Odoc
        fields = ('id', 'text', 'odoc_type')


class OdocListSerializer(DocumentListSerializer):
    url = serializers.HyperlinkedIdentityField(
        view_name='api:odoc-api:detail',
    )
    delete_url = serializers.HyperlinkedIdentityField(
        view_name='api:odoc-api:delete',
    )
    odoc_type = serializers.SerializerMethodField()
    text = serializers.SerializerMethodField()

    class Meta:
        model = Odoc
        fields = (
            'id',
            'text',
            'odoc_type',
            'author',
            'url',
            'delete_url',
            'created_at',
            'changed_at'
        )

    def get_odoc_type(self, obj):
        return OdocTypeSerializer(obj.odoc_type).data.get('title')

    def get_text(self, obj):
        return utils.get_title(obj.text)
