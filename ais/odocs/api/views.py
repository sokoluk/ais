from rest_framework import filters
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    CreateAPIView,
    UpdateAPIView,
    DestroyAPIView,
)

from odocs.models import Odoc
from odocs.models import OdocType
from odocs.models import Document

from .serializers import (
    OdocSerializer,
    OdocEditSerializer,
    OdocListSerializer,
    OdocTypeSerializer,
)
from .pagination import OdocLimitOffsetPagination
from documents.api.pagination import DocumentPageNumberPagination


class OdocTypeListAPIView(ListAPIView):
    queryset = OdocType.objects.all()
    serializer_class = OdocTypeSerializer


class OdocListAPIView(ListAPIView):
    """
    API endpoint that allowed odocs documents to be list view.
    """
    # queryset = Odoc.objects.all()
    serializer_class = OdocListSerializer
    pagination_class = DocumentPageNumberPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    search_fields = ('id', 'document__author__last_name', 'text')
    ordering_fields = '__all__'

    def get_queryset(self, *args, **kwargs):
        return Odoc.objects.all().select_related('document', 'document__author', 'odoc_type')


class OdocCreateAPIView(CreateAPIView):
    """
    API endpoint that odoc document to be updated.
    """
    queryset = Odoc.objects.all()
    serializer_class = OdocEditSerializer

    def perform_update(self, serializer):
        serializer.save(author=self.request.user)

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)


class OdocDetailAPIView(RetrieveAPIView):
    """
    API endpoint that odoc document to be viewed.
    """
    queryset = Odoc.objects.all()
    serializer_class = OdocSerializer


class OdocUpdateAPIView(UpdateAPIView):
    """
    API endpoint that odoc document to be updated.
    """
    queryset = Odoc.objects.all()
    serializer_class = OdocEditSerializer


class OdocDeleteAPIView(DestroyAPIView):
    """
    API endpoint that odoc document to be deleted.
    """
    queryset = Odoc.objects.all()
    serializer_class = OdocSerializer
