import json
from django.contrib.auth.models import User
from django.urls import reverse
from django.test import TestCase
from django.test import Client

from rest_framework import status
from rest_framework.test import force_authenticate
from rest_framework.test import APIRequestFactory

from . import views

class OdocsViewTests(TestCase):
    def setUp(self):
        self.request_factory = APIRequestFactory()
        self.client = Client()
        self.user = User.objects.create_user(
            username='test_user',
            email='',
            password='top_secret'
        )
        self.client.login(username='test_user', password='top_secret')

    def test_list_view(self):
        """
        try get list odocs
        """
        response = self.client.get(reverse('api:odoc-api:list'))
        self.assertEqual(response.status_code, 200)

    def test_add_odoc(self):
        """
        try add odoc
        """
        form_data = {
            'odoc_type': 1,
            'text': 'тестовый пример'
        }
        # Создание документа
        req = self.request_factory.post(
            reverse('api:odoc-api:create'),
            form_data
        )
        force_authenticate(req, user=self.user)
        create_rest = views.OdocCreateAPIView.as_view()(req)
        self.assertEqual(create_rest.status_code, status.HTTP_201_CREATED)
        # Вычитка
        req_list = self.request_factory.get(reverse('api:odoc-api:list'))
        response = views.OdocListAPIView.as_view()(req_list)
        response.render()
        resp = json.loads(response.content)
        self.assertEqual(resp.get('count'), 1)
        self.assertEqual(resp.get('results')[0].get('author'), 'test_user')
