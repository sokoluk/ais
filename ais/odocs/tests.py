from django.contrib.auth.models import User
from django.urls import reverse
from django.test import TestCase
from django.test import Client

from .forms import OdocForm


def create_odoc(odoc_type, text_content):
    """
    Creates odoc
    """
    return OdocForm.objects.create(odoc_type=2, text='тестовый пример')


class OdocsViewTests(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user(
            username='jacob',
            email='',
            password='top_secret'
        )
        self.client.force_login(self.user)

    def test_list_view(self):
        """
        try get list odocs
        """
        response = self.client.get(reverse('odocs:list'))
        self.assertEqual(response.status_code, 200)

    def test_add_odoc(self):
        """
        try add odoc
        """
        form_data = {
            'odoc_type': 1,
            'text': 'тестовый пример'
        }
        response = self.client.post("/odocs/create/", form_data)
        self.assertRedirects(response, reverse('odocs:edit', kwargs={'pk': 1}), 302, 200)
