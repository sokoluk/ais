from ckeditor.widgets import CKEditorWidget
from django import forms
from .models import (
    Odoc,
    OdocType,
)
from documents.models import Document
from documents.forms import DocumentForm


class OdocForm(DocumentForm):
    text = forms.CharField(
        widget=CKEditorWidget(),
        error_messages={'required': 'Введите текст служебной записки!'},
        label='Текст',
    )

    odoc_type = forms.ModelChoiceField(
        queryset=OdocType.objects.all(),
        label='Тип служебной записки',
    )

    class Meta:
        model = Odoc
        fields = ('odoc_type', 'text')
