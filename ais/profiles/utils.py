"""Common utils for Profile subsystems"""


def user_directory_path(instance, filename):
    """user avatar filepath

    Args:
        instance: Profile instance
        filename: (str) filename will be ignored
    """
    return 'user_{0}_{1}'.format(instance.user.id, 'avatar.png')
