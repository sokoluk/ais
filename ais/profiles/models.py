"""Extend User model with addition user property"""
from django.db import models
from django.dispatch import receiver
from mptt import models as mptt_models
from django.contrib.auth.models import User
from django.db.models.signals import post_save

from .utils import user_directory_path


class SubDivision(mptt_models.MPTTModel):
    """
    Subdivision of organisation
    """
    title = models.TextField(max_length=100)
    parent = mptt_models.TreeForeignKey(
        'self',
        null=True,
        blank=True,
        related_name='children',
        db_index=True,
        on_delete=models.CASCADE
    )
    def __str__(self):
        return 'Subdivision (pk =%r title=%r)' % (self.pk, self.title)


class Profile(models.Model):
    """Profile model"""
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    photo = models.FileField(upload_to=user_directory_path, null=True)
    location = models.CharField(max_length=30, null=True)
    birth_date = models.DateField(null=True)
    subdivision = models.ForeignKey('SubDivision', null=True, on_delete=models.PROTECT)

    @property
    def photo_url(self):
        if self.photo and hasattr(self.photo, 'url'):
            return self.photo.url

# Вешаем хуки на сохранение user и создания профиля
@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
