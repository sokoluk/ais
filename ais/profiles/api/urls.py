from django.conf.urls import url
from rest_framework_jwt.views import obtain_jwt_token

from . import views

urlpatterns = [
    url(r'^auth/token/$', obtain_jwt_token),
    url(r'^list/(?P<subdivision>\d+)/$', views.ProfileListAPIView.as_view(), name='profile_list'),
    url(r'^me/$', views.ProfileMeAPIView.as_view(), name='profile_me'),
    url(r'^list/$', views.ProfileListAPIView.as_view(), name='profile_list'),
    # url(r'^detail/(?P<pk>\d+)$', views.ProfileDetailAPIView.as_view(), name='profile_detail'),
    url(r'^(?P<user>\d+)/$', views.ProfileAPIView.as_view(), name='subdivision_detail'),
    url(r'^create/$', views.ProfileCreateAPIView.as_view(), name='profile_create'),
    url(r'^subdivision/$', views.SubDivisionListAPIView.as_view(), name='subdivision'),
    url(r'^subdivision/create/$', views.SubDivisionCreateAPIView.as_view(), name='subdivision_create'),
    url(r'^subdivision/(?P<pk>\d+)/$', views.SubDivisionAPIView.as_view(), name='subdivision_detail'),
]
