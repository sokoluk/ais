from rest_framework import serializers

from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

from profiles.models import SubDivision
from profiles.models import Profile


class SubDivisionCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = SubDivision
        fields = (
            'id', 'title', 'parent'
        )

    def get_parent(self, obj):
        if obj.parent:
            return SubDivisionSerializer(obj.parent).data
        return None


class SubDivisionSerializer(serializers.ModelSerializer):
    parent = serializers.SerializerMethodField()

    class Meta:
        model = SubDivision
        fields = (
            'id', 'title', 'parent'
        )

    def get_parent(self, obj):
        if obj.parent:
            return SubDivisionSerializer(obj.parent).data
        return None


class SubDivisionTreeSerializer(serializers.ModelSerializer):
    children = serializers.SerializerMethodField()

    class Meta:
        model = SubDivision
        fields = (
            'id', 'title', 'children'
        )

    def get_children(self, obj):
        if obj.children:
            return SubDivisionTreeSerializer(obj.children, many=True).data
        return None


class ProfileSerializer(serializers.ModelSerializer):
    short_name = serializers.SerializerMethodField()
    photo = serializers.SerializerMethodField()
    subdivision = serializers.SerializerMethodField()
    username = serializers.CharField(max_length=128)

    class Meta:
        model = User
        fields = (
            'id', 'username', 'email', 'first_name', 'last_name',
            'short_name', 'photo', 'subdivision'
        )

    def get_subdivision(self, obj):
        return SubDivisionSerializer(obj.profile.subdivision).data

    def get_photo(self, obj):
        return obj.profile.photo_url

    def get_short_name(self, obj):
        name = ''
        if obj.first_name:
            name = '{}.'.format(obj.first_name[0])
        return ' '.join((obj.last_name, name))


class ProfileCreateSerializer(serializers.ModelSerializer):
    user = ProfileSerializer()

    class Meta:
        model = Profile
        fields = (
            'photo', 'subdivision', 'user'
        )

    def create(self, validated_data):
        _user = validated_data.get('user')
        user = User.objects.create_user(**_user)
        if validated_data.get('password'):
            user.set_password(validated_data.pop('password'))
        user.save()
        _profile = user.profile
        _profile.photo = validated_data.get('photo')
        _profile.subdivision = validated_data.get('subdivision')
        _profile.save()
        return _profile

    def update(self, user_pk, validated_data):
        _user = validated_data.get('user')
        user = User.objects.get(pk=user_pk)
        if _user.get('username') != user.username:
            user2 = User.objects.get(username=_user.get('username'))
            if user2.pk != user_pk:
                raise ValidationError('Username already exists.')

        if validated_data.get('password'):
            user.set_password(validated_data.pop('password'))
        for _key in _user:
            setattr(user, _key, _user.get(_key))
        user.save()
        _profile = user.profile
        _profile.photo = validated_data.get('photo')
        _profile.subdivision = validated_data.get('subdivision')
        _profile.save()
        return _profile


class ProfileSimpleSerializer(ProfileSerializer):

    class Meta:
        model = User
        fields = (
            'id', 'username', 'email', 'first_name', 'last_name',
            'short_name', 'photo'
        )


class ProfileTreeSerializer(serializers.ModelSerializer):
    persons = serializers.SerializerMethodField()
    children = serializers.SerializerMethodField()

    class Meta:
        model = SubDivision
        fields = (
            'id', 'title', 'parent', 'persons', 'children', 'level'
        )

    def get_persons(self, obj):
        _persons = User.objects.all().select_related('profile').filter(
            profile__subdivision=obj.pk
        )
        return ProfileSimpleSerializer(_persons, many=True).data

    def get_children(self, obj):
        if obj.children:
            return SubDivisionSerializer(obj.children, many=True).data
        return None
