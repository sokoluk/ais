"""View for Profiles"""

from rest_framework import generics
from rest_framework.permissions import AllowAny
from django.contrib.auth.models import User

from profiles import models as profiles_models

from . import serializers


class ProfileListAPIView(generics.ListAPIView):
    """List Profiles App"""

    serializer_class = serializers.ProfileTreeSerializer
    def get_queryset(self):
        _id = self.kwargs.get('subdivision')
        if _id:
            return profiles_models.SubDivision.objects.all().filter(
                id=_id
            )
        else:
            return profiles_models.SubDivision.objects.all().filter(
                parent=None
            )

class ProfileMeAPIView(generics.RetrieveAPIView):
    """Profile current user detail"""
    serializer_class = serializers.ProfileCreateSerializer
    def get_object(self):
        print(self.request.user)
        if not self.request.user or self.request.user.is_anonymous():
            return None
        return profiles_models.Profile.objects.get(
            user=self.request.user
        )


class ProfileAPIView(generics.RetrieveUpdateAPIView):
    """Profile detail"""
    lookup_field = 'user'
    serializer_class = serializers.ProfileCreateSerializer
    queryset = profiles_models.Profile.objects.all()

    def perform_update(self, serializer):
        serializer.update(self.kwargs.get('user'), serializer.validated_data)


class ProfileCreateAPIView(generics.CreateAPIView):
    """Create profile detail"""
    serializer_class = serializers.ProfileCreateSerializer
    queryset = profiles_models.Profile.objects.all()


class SubDivisionCreateAPIView(generics.CreateAPIView):
    """Create subdivision view"""
    serializer_class = serializers.SubDivisionCreateSerializer
    queryset = profiles_models.SubDivision.objects.all()


class SubDivisionAPIView(generics.RetrieveAPIView):
    """Detail of subdivision"""
    serializer_class = serializers.SubDivisionSerializer
    queryset = profiles_models.SubDivision.objects.all()


class SubDivisionListAPIView(generics.ListAPIView):
    """List of subdivisions"""
    serializer_class = serializers.SubDivisionTreeSerializer
    queryset = profiles_models.SubDivision.objects.all().filter(
        parent=None
    )
