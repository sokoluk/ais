from django.conf.urls import url, include

from . import views

urlpatterns = [
    url('^logout-confirm/$', views.LogoutView.as_view(), name='logout_confirm'),
    url('^logout/$', views.logout_view, name='logout'),
    url('^', include('django.contrib.auth.urls')),
]
