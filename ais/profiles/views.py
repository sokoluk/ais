from django.contrib.auth import logout
from django.urls import reverse
from django.http import HttpResponseRedirect

from django.shortcuts import render
from django.views.generic import FormView

from .forms import LogoutForm


class LogoutView(FormView):
    form_class = LogoutForm
    template_name = 'profiles/logout.html'

    def form_valid(self, form):
        logout(self.request)
        return HttpResponseRedirect(reverse('home'))


def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('home'))
