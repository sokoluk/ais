from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import generic

from .forms import DashBoardForm
from odocs.models import Odoc

class DashBoardView(LoginRequiredMixin, generic.ListView):
    form_class = DashBoardForm
    model = Odoc
    template_name = 'dashboard/dashboard.html'

    def get_objects(self, queryset=None):
        return Odoc.objects.all()
