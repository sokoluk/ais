from django.conf.urls import url

from . import views

urlpatterns = [
    url('^dashboard/$', views.DashBoardView.as_view(), name='dashboard'),
]
