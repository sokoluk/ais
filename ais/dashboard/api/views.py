"""View for Dashboard"""
from rest_framework.generics import ListAPIView

from odocs.models import Odoc

from .serializers import OdocSerializer


class OdocViewSet(ListAPIView):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Odoc.objects.all()
    serializer_class = OdocSerializer
