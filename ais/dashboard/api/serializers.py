from rest_framework import serializers

from odocs.models import Odoc


class OdocSerializer(serializers.ModelSerializer):
    class Meta:
        model = Odoc
        fields = ('text', 'document', 'odoc_type', 'author')
