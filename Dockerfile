FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir -p /code
WORKDIR /code
ADD /ais/requirements.txt .
RUN pip install -r requirements.txt